package org.ow2.petals.perf.hbr.se;

import org.ow2.petals.component.framework.api.message.Exchange;
import org.ow2.petals.component.framework.jbidescriptor.generated.Consumes;
import org.ow2.petals.component.framework.util.UtilFactory;
import org.ow2.petals.se.eip.EIPConstants;
import org.ow2.petals.se.eip.ExchangeContext;
import org.ow2.petals.se.eip.async.CommonAsyncContext;
import org.ow2.petals.se.eip.patterns.AbstractMEPBridgeWithUnicConsumesPattern;
import org.ow2.petals.se.eip.patterns.PatternHelper;
import org.w3c.dom.Node;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author sampath
 * @since 1.5.0
 */
public class HeaderRouter extends AbstractMEPBridgeWithUnicConsumesPattern {

    public final static String ROUTER_NAMESPACE = EIPConstants.NAMESPACE + "/router";

    public void init() {}

    public String getName() {
        return "Router";
    }

    @Override
    public boolean process(Exchange exchange, ExchangeContext context) {

        try {
            String mode = context.getExtensions().get("mode");
            String headerName = context.getExtensions().get("header-name");
            String headerValue = context.getExtensions().get("header-value");

            boolean condition = false;

            if ("SOAP".equals(mode)) {
                condition = testSOAPHeader(headerName, headerValue, exchange);
            } else if ("TRANSPORT".equals(mode)) {
                // todo: transport header based routing cannot be done for the time being as the SOAP-BC doesn't pass in
                // todo: any clue as to what transport headers were present in the incoming message on the wire
                throw new UnsupportedOperationException("TRANSPORT mode for routing is not supported");
            } else {
                logger.log(Level.WARNING, "Invalid routing mode");
            }

            if (condition) {
                final List<Consumes> consumesList = context.getSUConsumes(exchange.getEndpoint());
                if (consumesList != null && !consumesList.isEmpty()) {
                    final Consumes consumedService = consumesList.get(0);
                    final Exchange routedExchange = context.createConsumeExchange(consumedService);
                    if (routedExchange.getOperation() == null) {
                        routedExchange.setOperation(exchange.getOperation());
                    }
                    PatternHelper.copy(exchange.getInMessage(), routedExchange.getInMessage());
                    UtilFactory.getExchangeUtil().copyExchangeProperties(exchange, routedExchange);

                    this.logSend(consumedService, routedExchange);

                    final boolean faultRobust = this.isFaultToException(context.getExtensions());
                    final CommonAsyncContext asyncContext = new CommonAsyncContext(exchange,
                        CommonAsyncContext.DEFAULT_TIME_TO_LIVE, this.getName(), 1, false,
                        false, faultRobust);

                    context.sendAsync(routedExchange, asyncContext);
                }
            }
        } catch (Exception e) {
            exchange.setError(e);
        }

        return false;
    }

    private boolean testSOAPHeader(String headerName, String headerValue, Exchange exchange) {
        Map headers = (Map) exchange.getInMessage().getProperty("javax.jbi.messaging.protocol.headers");
        if (headers != null) {
            Node headerNode = (Node) headers.get(headerName);
            if (headerNode != null) {
                return headerValue.trim().equals(headerNode.getTextContent().trim());
            }
        }
        return false;
    }

    @Override
    public boolean validateMEP(URI mep) {
        return true;
    }

    @Override
    public String getNameSpace() {
        return ROUTER_NAMESPACE;
    }
}
