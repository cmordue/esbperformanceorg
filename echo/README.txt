
This contains the src for the service.war that can be dropped into
tomcat to use as the backend service instead of the toolbox
echo service.  

There is also a server.xml for tomcat that configures a sizable 
thread pool and also turns off the access logging as we don't 
want the disk writes and such of the logging to affect performance
else where.

The server_nio.xml file will set tomcat 7 up to use the NIO connect 
which would allow the optional "delay" things in the servlet to
use the Servlet 3 AsyncContext stuff to implement the delay and
not consume the threads.


