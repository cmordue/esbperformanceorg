package org.apache.servicemix.perf.hbr.soap;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.w3c.dom.Element;

import javax.jbi.messaging.InOut;
import java.util.Map;

/**
 * @author sampath
 * @since 1.5.0
 */
public class HBRSoapPredicate implements Predicate {

    private String headerNS;
    private String headerLocalName;
    private String headerStringValue;

    private String headerQName;

    public HBRSoapPredicate(String headerNS, String headerLocalName, String headerStringValue) {
        this.headerNS = headerNS;
        this.headerLocalName = headerLocalName;
        this.headerStringValue = headerStringValue;
        this.headerQName = "{" + headerNS + "}" + headerLocalName;
    }

    public boolean matches(Exchange exchange) {
        boolean evalResult = false;
        InOut io = (InOut) exchange.getProperty("JbiMessageExchange");
        Map headers = (Map) io.getInMessage().getProperty("javax.jbi.messaging.protocol.headers");
        for (Object o : headers.keySet()) {
            if (headerQName.equals(o.toString())) {
                Element el = (Element) headers.get(o);
                evalResult = el.getTextContent().equals(headerStringValue);
                break;
            }
        }
        return evalResult;
    }

    public String getHeaderNS() {
        return headerNS;
    }

    public void setHeaderNS(String headerNS) {
        this.headerNS = headerNS;
    }

    public String getHeaderLocalName() {
        return headerLocalName;
    }

    public void setHeaderLocalName(String headerLocalName) {
        this.headerLocalName = headerLocalName;
    }

    public String getHeaderStringValue() {
        return headerStringValue;
    }

    public void setHeaderStringValue(String headerStringValue) {
        this.headerStringValue = headerStringValue;
    }
}