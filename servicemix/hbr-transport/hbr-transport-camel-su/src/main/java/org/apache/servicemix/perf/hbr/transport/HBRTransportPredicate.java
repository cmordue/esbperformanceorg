package org.apache.servicemix.perf.hbr.transport;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;

import javax.jbi.messaging.InOut;
import java.util.List;
import java.util.Map;

/**
 * @author sampath
 * @since 1.5.0
 */
public class HBRTransportPredicate implements Predicate {

    public HBRTransportPredicate(String headerName, String headerValue) {
        this.headerName = headerName;
        this.headerValue = headerValue;
    }

    private String headerName;
    private String headerValue;

    public boolean matches(Exchange exchange) {
        InOut io = (InOut) exchange.getProperty("JbiMessageExchange");
        Map headers = (Map) io.getInMessage().getProperty("org.apache.servicemix.perf.transport.headers");
        if (headers.containsKey(headerName)) {
            Object headerValue = ((List) headers.get(headerName)).get(0);
            return this.headerValue.equals(headerValue.toString());
        }
        return false;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getHeaderValue() {
        return headerValue;
    }

    public void setHeaderValue(String headerValue) {
        this.headerValue = headerValue;
    }
}
