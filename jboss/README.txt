JBoss ESB 4.11

Copy binary artifacts from the deploy directory to jbossesb-server-4.11/server/default/deploy
    
    $ cd ~/resources/esbperformance/jboss
    $ cp deploy/* {JBOSSESB_HOME}/server/default/deploy

e.g. 
    $ cd ~/resources/esbperformance/jboss
    $ cp deploy/* /home/ubuntu/esbs/jbossesb-server-4.11/server/default/deploy/

To build from source, copy quickstart directory contents into jbossesb-server-4.11/samples/quickstart and compile each with ant

Edit the ./server/default/deploy/jboss-web.deployer/server.xml file to tune the Tomcat thread pool, connections timeout and the accept count as follows

<!-- INCREASE maxThreads AND connectionTimeout and specify address-->
<Connector port="8080" address="0.0.0.0"    
maxThreads="300" maxHttpHeaderSize="8192"
emptySessionPath="true" protocol="HTTP/1.1"
enableLookups="false" redirectPort="8443" acceptCount="100"
connectionTimeout="120000" disableUploadTimeout="true" />

Turn off logging totally (as per [1] although this may not be possible on a real production environment) by editing ./server/default/conf/jboss-log4j.xml as follows:

<root>
<appender-ref ref="CONSOLE"/>
<!--COMMENT THIS LINE - appender-ref ref="FILE"/-->
</root>

<!-- UNCOMMENT THE FOLLOWING LINES - Limit JBoss categories-->
<category name="org.jboss">
<priority value="INFO"/>
</category>

Turn off hot deployment by disabling the directory scanning by editing ./server/default/conf/jboss-service.xml as follows:

<!-- SET TO "false" - A flag to disable the scans -->
<attribute name="ScanEnabled">false</attribute>

Increase heap memory allocated to the JVM by editing ./bin/run.conf as follows:

JAVA_OPTS="-Xms2048m -Xmx2048m -XX:PermSize=200M ......


Start the JBossESB by executing the start script as follows, and check for any warnings or errors

    $ cd {JBOSSESB_HOME}/bin
    $ ./run.sh
...
04:20:33,223 INFO  [Server] JBoss (MX MicroKernel) [4.2.3.GA (build: SVNTag=JBoss_4_2_3_GA date=200807181417)] Started in 23s:511ms

Running the complete performance test

    $ cd ~/client-scripts
    $ ./loadtest-jbossesb.sh > jbossesb-4.11.log

Notes:
======

During CBR on payload testing, we noticed that the response for a 5K request was 368 - i.e. a SOAP fault, indicating the XPath expression failed. However manual testing with the SOA ToolBox confirmed that randomly the XPath evaluation would pass and fail for the requests, we also noticed that when the load test was running, all XPath expressions were failing. We aborted further testing after this issue.


References

[1] JBossESB Performance Tuning - http://community.jboss.org/wiki/JBossESBPerformanceTuning
