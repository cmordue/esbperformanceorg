import org.jboss.soa.esb.actions.AbstractActionLifecycle;
import org.jboss.soa.esb.helpers.ConfigTree;
import org.jboss.soa.esb.message.*;
import org.jboss.soa.esb.actions.ActionProcessingException;
import java.util.Set;

public class SOAPFaultAction extends AbstractActionLifecycle {

	protected ConfigTree _config;

	public SOAPFaultAction(ConfigTree config) { _config = config; } 
	
	public Message process(Message msg) throws ActionProcessingException {
		
        msg.getBody().add("<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\"><S:Body><SOAP-ENV:Fault xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><faultcode xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns=\"\">SOAP-ENV:Server</faultcode><faultstring xmlns=\"\">" + _config.getAttribute("faultText") + "</faultstring></SOAP-ENV:Fault></S:Body></S:Envelope>");
		return msg;
	}
}
