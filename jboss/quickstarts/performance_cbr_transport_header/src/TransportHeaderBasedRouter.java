/*
 * JBoss, Home of Professional Open Source
 * Copyright 2006, JBoss Inc., and individual contributors as indicated
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
import java.util.*;
import org.jboss.soa.esb.message.*;
import org.apache.log4j.Logger;
import org.jboss.soa.esb.actions.*;
import org.jboss.soa.esb.Service;
import org.jboss.soa.esb.ConfigurationException;
import org.jboss.soa.esb.helpers.ConfigTree;
import org.jboss.soa.esb.services.registry.RegistryException;
import org.jboss.soa.esb.services.routing.MessageRouterException;
import org.jboss.soa.esb.message.mapping.ObjectMapper;
import org.jboss.soa.esb.message.mapping.ObjectMappingException;
import org.jboss.soa.esb.http.HttpRequest;
import org.jboss.soa.esb.client.MessageMulticaster;
import org.jboss.soa.esb.listeners.ListenerTagNames;
import java.util.regex.Pattern;

/**
 * Based on JBoss ESB samples and code base
 */
public class TransportHeaderBasedRouter extends ContentBasedWiretap {

    private Map<Pattern, Service> regexToServiceMap = new HashMap<Pattern, Service>();

    protected List<Service> executeRules(Message message) throws MessageRouterException {

        String value = HttpRequest.getRequest(message).getHeaderValue(_config.getAttribute("name"));
        List<Service> outgoingDestinations = new ArrayList<Service>();

        for (Map.Entry<Pattern, Service> e : regexToServiceMap.entrySet()) {
            if (e.getKey().matcher(value).matches()) {
                outgoingDestinations.add(e.getValue());
                break;
            }
        }
        return outgoingDestinations;
    }

    public TransportHeaderBasedRouter(ConfigTree _config) throws ConfigurationException, RegistryException, MessageRouterException {
        super(_config);

        ConfigTree[] destList = _config.getChildren(ROUTE_TO_TAG);
        if (destList != null) {
            for (ConfigTree curr : destList) {
                try {
                    String key = curr.getAttribute("regex");
                    String category = curr.getAttribute(ListenerTagNames.SERVICE_CATEGORY_NAME_TAG, "");
                    String name = curr.getRequiredAttribute(ListenerTagNames.SERVICE_NAME_TAG);
                    Service service = new Service(category, name);
                    regexToServiceMap.put(Pattern.compile(key), service);
                } catch (Exception e) {
                    throw new ConfigurationException("Problems with destination list", e);
                }
            }
        }
    }

    public Message process(Message message) throws ActionProcessingException {
        super.process(message) ;
        return null ;
    }
}
