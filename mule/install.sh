#!/bin/sh

muleDir=$1

usage() {
	echo "This will install the configuration on a given mule instance"
	echo "Usage: install.sh <mule instance directory>"
}

noMuleDir() {
	echo "The mule instance directory does not exist: ${muleDir}"
	echo "For help: install.sh"
}

install() {
	appDir=${muleDir}/apps/default
	backupDir=${muleDir}/`(date +%Y%m%d_%H%M%S)`
	echo "Preparing to install this configuration in ${appDir}"
	
	echo "Will first backup config to: ${backupDir}"
	mkdir -p ${backupDir}
	if [ ! -d ${backupDir} ]; then
		echo "Failed to create backup directory: ${backupDir}"
		echo "Is there a file permissions problem?"
		exit 1
	fi
	cp -r ${muleDir}/conf ${backupDir}
	backupAppDir=${backupDir}/apps/default
	mkdir -p ${backupAppDir}
	if [ ! -d ${backupDir} ]; then
		echo "Failed to create backup app directory: ${backupAppDir}"
		echo "Is there a file permissions problem?"
		exit 1
	fi
	cp -r ${appDir}/ ${backupAppDir}/
	backupSecureProxyLibDir=${backupDir}/lib/user
	mkdir -p ${backupSecureProxyLibDir}
	if [ ! -d ${backupSecureProxyLibDir} ]; then
		echo "Failed to create backup secure proxy lib directory: ${backupSecureProxyLibDir}"
		echo "Is there a file permissions problem?"
		exit 1
	fi
	secureProxyLibDir=${muleDir}/lib/user

	shopt -s nullglob; 
	secureProxyLibs=${secureProxyLibDir}/secure-proxy-additional-*.jar
	for f in ${secureProxyLibs}
	do
		cp ${f} ${backupSecureProxyLibDir}/
	done
	echo "backup is complete."

	echo "removing application dir and copying in new configuration."
	rm -r ${appDir}
	mkdir -p ${appDir}/classes

	DIR="$( cd "$( dirname "$0" )" && pwd )"
	cp ${DIR}/conf/log4j.properties ${appDir}/classes/
	cp ${DIR}/mule-config.xml ${appDir}/

	cp -r ${DIR}/conf/ ${muleDir}/conf/

	rm -f ${secureProxyLibDir}/secure-proxy-additional-*.jar
	cp ${DIR}/secure-proxy-additional-1.0.jar ${secureProxyLibDir}/
	echo "installation complete."
}

if [ -z ${muleDir} ]; then
	usage
	exit 0
elif [ ! -d ${muleDir} ]; then
	noMuleDir
	exit 1
else
	install
fi