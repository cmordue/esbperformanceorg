Mule CE 3.3.0

Copy the content in the conf directory into the Mule conf directory

    $ cd ~/resources/esbperformance/mule
    $ cp conf/* {MULE_HOME}/conf/

e.g.
    $ cd ~/resources/esbperformance/mule
    $ cp conf/* /home/ubuntu/esbs/mule-standalone-3.3.0/conf/

For the WS-Security proxy we need a password callback class. To build it using Maven

    $ cd secure-proxy
    $ mvn clean install

After building it copy the generated artifact (secure-proxy-additional-1.0.jar) into the mule user libraries directory

    $ cp target/secure-proxy-additional-1.0.jar {MULE_HOME}/lib/user/

If you do not wish to rebulid this JAR, use the one we have shipped

    $ cp secure-proxy-additional-1.0.jar {MULE_HOME}/lib/user/

e.g.
    $ cp secure-proxy-additional-1.0.jar /home/ubuntu/esbs/mule-standalone-3.3.0/lib/user/

Finally copy the mule configuraiton into the default application configuration of Mule

    $ cp mule-config.xml {MULE_HOME}/apps/default/

e.g.
    $ cp mule-config.xml /home/ubuntu/esbs/mule-standalone-3.3.0/apps/default

Edit the conf/log4j.properties file and add the following to turn off logs printed at INFO
    
    log4j.logger.org.mule=WARN

Additionally, configure the application's log by copying an appropriate log4j.properties into the application's classes folder

	$ mkdir -p {MULE_HOME}/apps/default/classes/
	$ cp conf/log4j.properties {MULE_HOME}/apps/default/classes/

e.g.
	$ mkdir -p /home/ubuntu/esbs/mule-standalone-3.3.0/apps/default/classes/
	$ cp conf/log4j.properties /home/ubuntu/esbs/mule-standalone-3.3.0/apps/default/classes/


Increase the Heap to 2G by editing {MULE_HOME}/conf/wrapper.conf to read as follows:

    # Initial Java Heap Size (in MB)
    wrapper.java.initmemory=2048
    # Maximum Java Heap Size (in MB)
    wrapper.java.maxmemory=2048

Edit {MULE_HOME}/conf/jetty.xml to increase thread pool sizes to read as follows. The performance tests do not ramp up the concurrency so thread creation time impacts the results:

    <Set name="minThreads">100</Set>
    <Set name="maxThreads">100</Set>


Now you are ready to start the Mule ESB server with all the performance scenarios. Start mule with the startup script.

    $ cd {MULE_HOME}
    $ bin/mule

** Note: Execute mule from the {MULE_HOME} directory

Before executing the load test, recreate the sample secure requests

    $ cd ~/client-scripts
    $ ./recreate-secure-requests.sh
    
    $ ./loadtest.sh http://localhost:8090/service > mule-3.3.0.txt


Service URLs
============

Direct Proxy
    url             : http://localhost:8090/service/DirectProxy
    wsdl-url        : http://localhost:8090/service/DirectProxy?wsdl

SOAP Body CBR Proxy
    url             : http://localhost:8090/service/CBRProxy
    wsdl-url        : http://localhost:8090/service/CBRProxy?wsdl

SOAP Header CBR Proxy
    url             : http://localhost:8090/service/CBRSOAPHeaderProxy
    wsdl-url        : http://localhost:8090/service/CBRSOAPHeaderProxy?wsdl

Transport Header CBR Proxy
    url             : http://localhost:8090/service/CBRTransportHeaderProxy
    wsdl-url        : http://localhost:8090/service/CBRTransportHeaderProxy?wsdl

XSLT Transformation Proxy
    url             : http://localhost:8090/service/XSLTProxy
    wsdl-url        : http://localhost:8090/service/XSLTProxy?wsdl

WS-Security Proxy
    url             : http://localhost:8090/service/SecureProxy
    wsdl-url        : http://localhost:8090/service/SecureProxy?wsdl

Note:
=====

While running the Mule test, we noticed that a tiny smoke test of a single request by a concurrency level of 1 passed, and wrote INFO level logs to the console. Subsequent runs of the same tiny test did not create console logs. However, when the concurrency and iterations were both increased to 10, Mule started generating lots of log entries at INFO level on the console, which we had to later turn off.

INFO  2012-08-02 13:40:52,751 [1443657750@qtp-524857970-31] org.mule.transport.service.DefaultTransportServiceDescriptor: Loading default outbound transformer: org.mule.transport.http.transformers.ObjectToHttpClientMethodRequest
INFO  2012-08-02 13:40:52,751 [593224478@qtp-524857970-34] org.mule.transport.service.DefaultTransportServiceDescriptor: Loading default outbound transformer: org.mule.transport.http.transformers.ObjectToHttpClientMethodRequest
INFO  2012-08-02 13:40:52,751 [1443657750@qtp-524857970-31] org.mule.transport.service.DefaultTransportServiceDescriptor: Loading default response transformer: org.mule.transport.http.transformers.MuleMessageToHttpResponse
INFO  2012-08-02 13:40:52,751 [593224478@qtp-524857970-34] org.mule.lifecycle.AbstractLifecycleManager: Initialising: 'HttpConnector.dispatcher.655601659'. Object is: HttpClientMessageDispatcher
INFO  2012-08-02 13:40:52,751 [1443657750@qtp-524857970-31] org.mule.transport.service.DefaultTransportServiceDescriptor: Loading default outbound transformer: org.mule.transport.http.transformers.ObjectToHttpClientMethodRequest
INFO  2012-08-02 13:40:52,751 [593224478@qtp-524857970-34] org.mule.lifecycle.AbstractLifecycleManager: Starting: 'HttpConnector.dispatcher.655601659'. Object is: HttpClientMessageDispatcher
INFO  2012-08-02 13:40:52,752 [1443657750@qtp-524857970-31] org.mule.lifecycle.AbstractLifecycleManager: Initialising: 'HttpConnector.dispatcher.1739432013'. Object is: HttpClientMessageDispatcher
INFO  2012-08-02 13:40:52,752 [1443657750@qtp-524857970-31] org.mule.lifecycle.AbstractLifecycleManager: Starting: 'HttpConnector.dispatcher.1739432013'. Object is: HttpClientMessageDispatcher

The following message was printed on each message to the log file at INFO level and could not be turned off via the log4j configuration
INFO  2012-08-04 20:02:10,615 [1566364831@qtp-260797080-39] org.mule.module.cxf.WSProxyService: Using file conf/ProxyWSDL-embedded.wsdl as WSDL file

During the final runs, we noticed the following error listed on the log files, that reported an error against the 10K Secure requests, although the messages themselves seemed to have been replied successfully.

WARN  2012-08-04 19:35:48,714 [1317216854@qtp-520443435-390] org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor: 
org.apache.ws.security.WSSecurityException: The signature or decryption was invalid
	at org.apache.ws.security.processor.SignatureProcessor.verifyXMLSignature(SignatureProcessor.java:373)
	at org.apache.ws.security.processor.SignatureProcessor.handleToken(SignatureProcessor.java:172)
	at org.apache.ws.security.WSSecurityEngine.processSecurityHeader(WSSecurityEngine.java:396)
	at org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor.handleMessage(WSS4JInInterceptor.java:249)
	at org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor.handleMessage(WSS4JInInterceptor.java:85)
	at org.apache.cxf.phase.PhaseInterceptorChain.doIntercept(PhaseInterceptorChain.java:263)
	at org.apache.cxf.transport.ChainInitiationObserver.onMessage(ChainInitiationObserver.java:123)
	at org.mule.module.cxf.CxfInboundMessageProcessor.sendToDestination(CxfInboundMessageProcessor.java:295)
	at org.mule.module.cxf.CxfInboundMessageProcessor.process(CxfInboundMessageProcessor.java:136)
	at org.mule.module.cxf.config.FlowConfiguringMessageProcessor.process(FlowConfiguringMessageProcessor.java:50)
	at org.mule.execution.ExceptionToMessagingExceptionExecutionInterceptor.execute(ExceptionToMessagingExceptionExecutionInterceptor.java:27)
	at org.mule.execution.MessageProcessorNotificationExecutionInterceptor.execute(MessageProcessorNotificationExecutionInterceptor.java:43)
	at org.mule.execution.MessageProcessorExecutionTemplate.execute(MessageProcessorExecutionTemplate.java:43)
	at org.mule.processor.chain.SimpleMessageProcessorChain.doProcess(SimpleMessageProcessorChain.java:47)
	at org.mule.processor.chain.AbstractMessageProcessorChain.process(AbstractMessageProcessorChain.java:66)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.doProcess(InterceptingChainLifecycleWrapper.java:57)
	at org.mule.processor.chain.AbstractMessageProcessorChain.process(AbstractMessageProcessorChain.java:66)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.access$001(InterceptingChainLifecycleWrapper.java:29)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper$1.process(InterceptingChainLifecycleWrapper.java:90)
	at org.mule.execution.ExceptionToMessagingExceptionExecutionInterceptor.execute(ExceptionToMessagingExceptionExecutionInterceptor.java:27)
	at org.mule.execution.MessageProcessorNotificationExecutionInterceptor.execute(MessageProcessorNotificationExecutionInterceptor.java:43)
	at org.mule.execution.MessageProcessorExecutionTemplate.execute(MessageProcessorExecutionTemplate.java:43)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.process(InterceptingChainLifecycleWrapper.java:85)
	at org.mule.execution.ExceptionToMessagingExceptionExecutionInterceptor.execute(ExceptionToMessagingExceptionExecutionInterceptor.java:27)
	at org.mule.execution.MessageProcessorNotificationExecutionInterceptor.execute(MessageProcessorNotificationExecutionInterceptor.java:43)
	at org.mule.execution.MessageProcessorExecutionTemplate.execute(MessageProcessorExecutionTemplate.java:43)
	at org.mule.processor.chain.SimpleMessageProcessorChain.doProcess(SimpleMessageProcessorChain.java:47)
	at org.mule.processor.chain.AbstractMessageProcessorChain.process(AbstractMessageProcessorChain.java:66)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.doProcess(InterceptingChainLifecycleWrapper.java:57)
	at org.mule.processor.chain.AbstractMessageProcessorChain.process(AbstractMessageProcessorChain.java:66)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.access$001(InterceptingChainLifecycleWrapper.java:29)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper$1.process(InterceptingChainLifecycleWrapper.java:90)
	at org.mule.execution.ExceptionToMessagingExceptionExecutionInterceptor.execute(ExceptionToMessagingExceptionExecutionInterceptor.java:27)
	at org.mule.execution.MessageProcessorNotificationExecutionInterceptor.execute(MessageProcessorNotificationExecutionInterceptor.java:43)
	at org.mule.execution.MessageProcessorExecutionTemplate.execute(MessageProcessorExecutionTemplate.java:43)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.process(InterceptingChainLifecycleWrapper.java:85)
	at org.mule.transport.AbstractMessageReceiver.routeMessage(AbstractMessageReceiver.java:192)
	at org.mule.transport.AbstractMessageReceiver.routeMessage(AbstractMessageReceiver.java:174)
	at org.mule.transport.AbstractMessageReceiver.routeMessage(AbstractMessageReceiver.java:166)
	at org.mule.transport.AbstractMessageReceiver.routeMessage(AbstractMessageReceiver.java:153)
	at org.mule.transport.servlet.MuleReceiverServlet.routeMessage(MuleReceiverServlet.java:261)
	at org.mule.transport.servlet.MuleReceiverServlet.doAllMethods(MuleReceiverServlet.java:248)
	at org.mule.transport.servlet.MuleReceiverServlet.doPost(MuleReceiverServlet.java:198)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:727)
	at org.mule.transport.servlet.MuleReceiverServlet.service(MuleReceiverServlet.java:180)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)
	at org.mortbay.jetty.servlet.ServletHolder.handle(ServletHolder.java:511)
	at org.mortbay.jetty.servlet.ServletHandler.handle(ServletHandler.java:401)
	at org.mortbay.jetty.handler.ContextHandler.handle(ContextHandler.java:766)
	at org.mortbay.jetty.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:230)
	at org.mortbay.jetty.handler.HandlerWrapper.handle(HandlerWrapper.java:152)
	at org.mortbay.jetty.Server.handle(Server.java:326)
	at org.mortbay.jetty.HttpConnection.handleRequest(HttpConnection.java:542)
	at org.mortbay.jetty.HttpConnection$RequestHandler.content(HttpConnection.java:945)
	at org.mortbay.jetty.HttpParser.parseNext(HttpParser.java:756)
	at org.mortbay.jetty.HttpParser.parseAvailable(HttpParser.java:218)
	at org.mortbay.jetty.HttpConnection.handle(HttpConnection.java:404)
	at org.mortbay.io.nio.SelectChannelEndPoint.run(SelectChannelEndPoint.java:410)
	at org.mortbay.thread.QueuedThreadPool$PoolThread.run(QueuedThreadPool.java:582)
Caused by: javax.xml.crypto.dsig.XMLSignatureException: javax.xml.crypto.URIReferenceException: com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolverException: Cannot resolve element with ID id-24697927507258-947370345
	at org.jcp.xml.dsig.internal.dom.DOMReference.dereference(DOMReference.java:352)
	at org.jcp.xml.dsig.internal.dom.DOMReference.validate(DOMReference.java:311)
	at org.jcp.xml.dsig.internal.dom.DOMXMLSignature.validate(DOMXMLSignature.java:244)
	at org.apache.ws.security.processor.SignatureProcessor.verifyXMLSignature(SignatureProcessor.java:348)
	... 58 more
Caused by: javax.xml.crypto.URIReferenceException: com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolverException: Cannot resolve element with ID id-24697927507258-947370345
	at org.jcp.xml.dsig.internal.dom.DOMURIDereferencer.dereference(DOMURIDereferencer.java:82)
	at org.jcp.xml.dsig.internal.dom.DOMReference.dereference(DOMReference.java:344)
	... 61 more
Caused by: com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolverException: Cannot resolve element with ID id-24697927507258-947370345
	at com.sun.org.apache.xml.internal.security.utils.resolver.implementations.ResolverFragment.engineResolve(ResolverFragment.java:89)
	at com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolver.resolve(ResourceResolver.java:236)
	at org.jcp.xml.dsig.internal.dom.DOMURIDereferencer.dereference(DOMURIDereferencer.java:75)
	... 62 more
WARN  2012-08-04 19:35:48,716 [1317216854@qtp-520443435-390] org.apache.cxf.phase.PhaseInterceptorChain: Interceptor for {http://services.samples/xsd}SecureProxy has thrown exception, unwinding now
org.apache.cxf.binding.soap.SoapFault: The signature or decryption was invalid
	at org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor.createSoapFault(WSS4JInInterceptor.java:641)
	at org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor.handleMessage(WSS4JInInterceptor.java:308)
	at org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor.handleMessage(WSS4JInInterceptor.java:85)
	at org.apache.cxf.phase.PhaseInterceptorChain.doIntercept(PhaseInterceptorChain.java:263)
	at org.apache.cxf.transport.ChainInitiationObserver.onMessage(ChainInitiationObserver.java:123)
	at org.mule.module.cxf.CxfInboundMessageProcessor.sendToDestination(CxfInboundMessageProcessor.java:295)
	at org.mule.module.cxf.CxfInboundMessageProcessor.process(CxfInboundMessageProcessor.java:136)
	at org.mule.module.cxf.config.FlowConfiguringMessageProcessor.process(FlowConfiguringMessageProcessor.java:50)
	at org.mule.execution.ExceptionToMessagingExceptionExecutionInterceptor.execute(ExceptionToMessagingExceptionExecutionInterceptor.java:27)
	at org.mule.execution.MessageProcessorNotificationExecutionInterceptor.execute(MessageProcessorNotificationExecutionInterceptor.java:43)
	at org.mule.execution.MessageProcessorExecutionTemplate.execute(MessageProcessorExecutionTemplate.java:43)
	at org.mule.processor.chain.SimpleMessageProcessorChain.doProcess(SimpleMessageProcessorChain.java:47)
	at org.mule.processor.chain.AbstractMessageProcessorChain.process(AbstractMessageProcessorChain.java:66)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.doProcess(InterceptingChainLifecycleWrapper.java:57)
	at org.mule.processor.chain.AbstractMessageProcessorChain.process(AbstractMessageProcessorChain.java:66)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.access$001(InterceptingChainLifecycleWrapper.java:29)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper$1.process(InterceptingChainLifecycleWrapper.java:90)
	at org.mule.execution.ExceptionToMessagingExceptionExecutionInterceptor.execute(ExceptionToMessagingExceptionExecutionInterceptor.java:27)
	at org.mule.execution.MessageProcessorNotificationExecutionInterceptor.execute(MessageProcessorNotificationExecutionInterceptor.java:43)
	at org.mule.execution.MessageProcessorExecutionTemplate.execute(MessageProcessorExecutionTemplate.java:43)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.process(InterceptingChainLifecycleWrapper.java:85)
	at org.mule.execution.ExceptionToMessagingExceptionExecutionInterceptor.execute(ExceptionToMessagingExceptionExecutionInterceptor.java:27)
	at org.mule.execution.MessageProcessorNotificationExecutionInterceptor.execute(MessageProcessorNotificationExecutionInterceptor.java:43)
	at org.mule.execution.MessageProcessorExecutionTemplate.execute(MessageProcessorExecutionTemplate.java:43)
	at org.mule.processor.chain.SimpleMessageProcessorChain.doProcess(SimpleMessageProcessorChain.java:47)
	at org.mule.processor.chain.AbstractMessageProcessorChain.process(AbstractMessageProcessorChain.java:66)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.doProcess(InterceptingChainLifecycleWrapper.java:57)
	at org.mule.processor.chain.AbstractMessageProcessorChain.process(AbstractMessageProcessorChain.java:66)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.access$001(InterceptingChainLifecycleWrapper.java:29)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper$1.process(InterceptingChainLifecycleWrapper.java:90)
	at org.mule.execution.ExceptionToMessagingExceptionExecutionInterceptor.execute(ExceptionToMessagingExceptionExecutionInterceptor.java:27)
	at org.mule.execution.MessageProcessorNotificationExecutionInterceptor.execute(MessageProcessorNotificationExecutionInterceptor.java:43)
	at org.mule.execution.MessageProcessorExecutionTemplate.execute(MessageProcessorExecutionTemplate.java:43)
	at org.mule.processor.chain.InterceptingChainLifecycleWrapper.process(InterceptingChainLifecycleWrapper.java:85)
	at org.mule.transport.AbstractMessageReceiver.routeMessage(AbstractMessageReceiver.java:192)
	at org.mule.transport.AbstractMessageReceiver.routeMessage(AbstractMessageReceiver.java:174)
	at org.mule.transport.AbstractMessageReceiver.routeMessage(AbstractMessageReceiver.java:166)
	at org.mule.transport.AbstractMessageReceiver.routeMessage(AbstractMessageReceiver.java:153)
	at org.mule.transport.servlet.MuleReceiverServlet.routeMessage(MuleReceiverServlet.java:261)
	at org.mule.transport.servlet.MuleReceiverServlet.doAllMethods(MuleReceiverServlet.java:248)
	at org.mule.transport.servlet.MuleReceiverServlet.doPost(MuleReceiverServlet.java:198)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:727)
	at org.mule.transport.servlet.MuleReceiverServlet.service(MuleReceiverServlet.java:180)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)
	at org.mortbay.jetty.servlet.ServletHolder.handle(ServletHolder.java:511)
	at org.mortbay.jetty.servlet.ServletHandler.handle(ServletHandler.java:401)
	at org.mortbay.jetty.handler.ContextHandler.handle(ContextHandler.java:766)
	at org.mortbay.jetty.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:230)
	at org.mortbay.jetty.handler.HandlerWrapper.handle(HandlerWrapper.java:152)
	at org.mortbay.jetty.Server.handle(Server.java:326)
	at org.mortbay.jetty.HttpConnection.handleRequest(HttpConnection.java:542)
	at org.mortbay.jetty.HttpConnection$RequestHandler.content(HttpConnection.java:945)
	at org.mortbay.jetty.HttpParser.parseNext(HttpParser.java:756)
	at org.mortbay.jetty.HttpParser.parseAvailable(HttpParser.java:218)
	at org.mortbay.jetty.HttpConnection.handle(HttpConnection.java:404)
	at org.mortbay.io.nio.SelectChannelEndPoint.run(SelectChannelEndPoint.java:410)
	at org.mortbay.thread.QueuedThreadPool$PoolThread.run(QueuedThreadPool.java:582)
Caused by: org.apache.ws.security.WSSecurityException: The signature or decryption was invalid
	at org.apache.ws.security.processor.SignatureProcessor.verifyXMLSignature(SignatureProcessor.java:373)
	at org.apache.ws.security.processor.SignatureProcessor.handleToken(SignatureProcessor.java:172)
	at org.apache.ws.security.WSSecurityEngine.processSecurityHeader(WSSecurityEngine.java:396)
	at org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor.handleMessage(WSS4JInInterceptor.java:249)
	... 55 more
Caused by: javax.xml.crypto.dsig.XMLSignatureException: javax.xml.crypto.URIReferenceException: com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolverException: Cannot resolve element with ID id-24697927507258-947370345
	at org.jcp.xml.dsig.internal.dom.DOMReference.dereference(DOMReference.java:352)
	at org.jcp.xml.dsig.internal.dom.DOMReference.validate(DOMReference.java:311)
	at org.jcp.xml.dsig.internal.dom.DOMXMLSignature.validate(DOMXMLSignature.java:244)
	at org.apache.ws.security.processor.SignatureProcessor.verifyXMLSignature(SignatureProcessor.java:348)
	... 58 more
Caused by: javax.xml.crypto.URIReferenceException: com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolverException: Cannot resolve element with ID id-24697927507258-947370345
	at org.jcp.xml.dsig.internal.dom.DOMURIDereferencer.dereference(DOMURIDereferencer.java:82)
	at org.jcp.xml.dsig.internal.dom.DOMReference.dereference(DOMReference.java:344)
	... 61 more
Caused by: com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolverException: Cannot resolve element with ID id-24697927507258-947370345
	at com.sun.org.apache.xml.internal.security.utils.resolver.implementations.ResolverFragment.engineResolve(ResolverFragment.java:89)
	at com.sun.org.apache.xml.internal.security.utils.resolver.ResourceResolver.resolve(ResourceResolver.java:236)
	at org.jcp.xml.dsig.internal.dom.DOMURIDereferencer.dereference(DOMURIDereferencer.java:75)
	... 62 more

