package org.fuse.esb.perf.hbr.transport.interceptor;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;

/**
 * @author sampath
 * @since 1.5.0
 */
public class TransportHeaderPassingInterceptor extends AbstractSoapInterceptor {

    public TransportHeaderPassingInterceptor() {
        super(Phase.PRE_INVOKE);
    }

    public void handleMessage(SoapMessage soapMessage) throws Fault {

        Object headers = soapMessage.get(Message.PROTOCOL_HEADERS);
        if (headers != null) {
            MessageExchange exchange = soapMessage.getContent(MessageExchange.class);
            NormalizedMessage norMessage = exchange.getMessage("in");
            norMessage.setProperty("org.fuse.esb.perf.transport.headers", headers);
        }
    }
}
